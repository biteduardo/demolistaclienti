﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoListaClienti
{
    class clienteCombo
    {
        public int id;
        public String nome;

        public clienteCombo(int _id, String _nome)
        {
            id = _id;
            nome = _nome;
        }

        public override String ToString()
        {
            return "stringa qualsiasi " + id + " " + nome;
        }
    }
}
