﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Windows.Forms;

namespace DemoListaClienti
{
    public partial class frmListaAcquirenti : Form
    {
        MySqlConnection conn;
        MySqlCommand cmd;
        MySqlDataAdapter adapter;
        DataTable dt;
        DataGridViewCellStyle style;
        string connectionString, query;

        public frmListaAcquirenti()
        {
            InitializeComponent();
            refresh();
            btnElimAcquirente.Enabled = false;
            btnModAcquirente.Enabled = false;
        }

        private void populate(String id, String nome, string indirizzo, string cap, string citta, string provincia, string longitudine, string latitudine)
        {

            longitudine = longitudine.ToString().Replace(",", ".");
            latitudine = latitudine.ToString().Replace(",", ".");


            dataGridView1.Rows.Add(id, nome, indirizzo, cap, citta, provincia, longitudine, latitudine);

        }

        private void refresh()
        {
            connectionString = "Server=localhost;Database=domodatabase;Uid=root;Pwd=eduardo;";
            conn = new MySqlConnection(connectionString);
            dt = new DataTable();
            dataGridView1.Rows.Clear();
            query = "SELECT * FROM acquirenti ";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();
                adapter = new MySqlDataAdapter(cmd);
                adapter.Fill(dt);



                /*dataGridView1.Columns[0].ValueType = GetType();
                dataGridView1.Columns[0].DefaultCellStyle.Format = "N6";*/

                foreach (DataRow row in dt.Rows)
                {
                    populate(row[0].ToString(), row[1].ToString(), row[2].ToString(), row[3].ToString(),
                        row[4].ToString(), row[5].ToString(), row[6].ToString(), row[7].ToString());
                }
                conn.Close();
                dt.Rows.Clear();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (dataGridView1.CurrentCell != null)
                    dataGridView1.CurrentCell.Selected = false;
                textBox1.Text = textBox2.Text = textBox3.Text = textBox4.Text = textBox5.Text = textBox6.Text = textBox7.Text = textBox8.Text = "";
                conn.Close();
            }
        }




        private void refreshData(object sender, EventArgs e)
        {
            refresh();            
        }

        private void btnInsAcquirente_Click(object sender, EventArgs e)
        {
            InsAcquirente insAcquirenteForm = new InsAcquirente();
            insAcquirenteForm.DataChanged += refreshData;
            insAcquirenteForm.Show();
            btnElimAcquirente.Enabled = false;
            btnModAcquirente.Enabled = false;
        }

        private void btnVisNodi_Click(object sender, EventArgs e)
        {            
            frmListaNodi visNodiForm = new frmListaNodi();
            visNodiForm.Show();
        }

        private void btnVisPeriodi_Click(object sender, EventArgs e)
        {
            frmListaPeriodi visPeriodiForm = new frmListaPeriodi();
            visPeriodiForm.Show();
        }

        private void delete(int id)
        {
            connectionString = "Server=localhost;Database=domodatabase;Uid=root;Pwd=eduardo;";
            conn = new MySqlConnection(connectionString);       
            dt = new DataTable();
            query = "DELETE FROM acquirenti WHERE ID=" + id + "";
            cmd = new MySqlCommand(query, conn);
            try
            {
                conn.Open();   
                adapter = new MySqlDataAdapter(cmd);
                adapter.DeleteCommand = conn.CreateCommand();
                adapter.DeleteCommand.CommandText = query;
                if (MessageBox.Show("Conferma eliminazione ID "+id.ToString(), "DELETE", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                {
                    if (cmd.ExecuteNonQuery() > 0)
                    {
                        MessageBox.Show("ID " + id.ToString() + " eliminato");
                        refresh();
                    }
                }
                conn.Close();           
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                conn.Close();
            }
        }

        private void btnElimAcquirente_Click(object sender, EventArgs e)
        {
            String selected = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            int id = Convert.ToInt32(selected);
            delete(id);
            refresh();
            btnElimAcquirente.Enabled = false;
            btnModAcquirente.Enabled = false;
        }

        private void update(int id)
        {            
            connectionString = "Server=localhost;Database=domodatabase;Uid=root;Pwd=eduardo;";
            conn = new MySqlConnection(connectionString);

            string nome, indirizzo, citta, provincia;
            int cap;
            string latitudine, longitudine;
            

            if (textBox2.Text == null || textBox2.Text == "")
                nome = "";
            else
                nome = textBox2.Text;
            if (textBox3.Text == null || textBox3.Text == "")
                indirizzo = "";
            else
                indirizzo = textBox3.Text;
            if (textBox4.Text == "0" || textBox4.Text == null || textBox4.Text == "")
                cap = 0;
            else
                cap = Convert.ToInt32(textBox4.Text);
            if (textBox5.Text == null || textBox5.Text == "")
                citta = "";
            else
                citta = textBox5.Text;
            if (textBox6.Text == null || textBox6.Text == "")
                provincia = "";
            else
                provincia = textBox6.Text;
            if (textBox7.Text == null || textBox7.Text == "")
                longitudine = "";
            else
                longitudine = textBox7.Text;
                //longitudine = Convert.ToDouble(textBox7.Text);
            if (textBox8.Text == null || textBox8.Text == "")
                latitudine = "";
            else
                latitudine = textBox8.Text;
                //latitudine = Convert.ToDouble(textBox8.Text);

            query = "UPDATE acquirenti SET " +
                "nome='" + nome +
                "',indirizzo='" + indirizzo +
                "',cap=" + cap +
                ",citta='" + citta +
                "',provincia='" + provincia +
                "',longitudine='" + longitudine +
                "',latitudine='" + latitudine +
                "' WHERE id=" + id + "";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();
                adapter = new MySqlDataAdapter(cmd);
                adapter.UpdateCommand = conn.CreateCommand();
                adapter.UpdateCommand.CommandText = query;
                if (cmd.ExecuteNonQuery() > 0)
                {
                     MessageBox.Show("Dati acquirente " + id.ToString() + " aggiornati");
                     refresh();
                }                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);                
            }
            finally
            {
                conn.Close();
            }
        }

        private void btnModAcquirente_Click(object sender, EventArgs e)
        {
            String selected = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            int id = Convert.ToInt32(selected);
            update(id);
            btnElimAcquirente.Enabled = false;
            btnModAcquirente.Enabled = false;
      


            /* 
            String selected = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            int id = Convert.ToInt32(selected);
            update(id);
            btnElimAcquirente.Enabled = false;
            btnModAcquirente.Enabled = false;*/
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int indexRow = e.RowIndex;
            if (indexRow < 0)
                return;
            DataGridViewRow row = dataGridView1.Rows[indexRow];
            textBox1.Text = row.Cells[0].Value.ToString();
            textBox2.Text = row.Cells[1].Value.ToString();
            textBox3.Text = row.Cells[2].Value.ToString();
            textBox4.Text = row.Cells[3].Value.ToString();
            textBox5.Text = row.Cells[4].Value.ToString();
            textBox6.Text = row.Cells[5].Value.ToString();
            textBox7.Text = row.Cells[6].Value.ToString();
            textBox8.Text = row.Cells[7].Value.ToString();
            btnModAcquirente.Enabled = true;
            btnElimAcquirente.Enabled = true;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int indexRow = e.RowIndex;
            if (indexRow < 0)
                return;
            DataGridViewRow row = dataGridView1.Rows[indexRow];
            textBox1.Text = row.Cells[0].Value.ToString();
            textBox2.Text = row.Cells[1].Value.ToString();
            textBox3.Text = row.Cells[2].Value.ToString();
            textBox4.Text = row.Cells[3].Value.ToString();
            textBox5.Text = row.Cells[4].Value.ToString();
            textBox6.Text = row.Cells[5].Value.ToString();
            textBox7.Text = row.Cells[6].Value.ToString();
            textBox8.Text = row.Cells[7].Value.ToString();
            btnModAcquirente.Enabled = true;
            btnElimAcquirente.Enabled = true;
        }
        
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        
        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }
        
        private void frmListaAcquirenti_Load(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, MouseEventArgs e)
        {

        }

       

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ModAcquirente modAcquirenteForm = new ModAcquirente();
            modAcquirenteForm.textBox8.Text = this.dataGridView1.CurrentRow.Cells[0].Value.ToString();
            modAcquirenteForm.textBox1.Text = this.dataGridView1.CurrentRow.Cells[1].Value.ToString();
            modAcquirenteForm.textBox2.Text = this.dataGridView1.CurrentRow.Cells[2].Value.ToString();
            modAcquirenteForm.textBox3.Text = this.dataGridView1.CurrentRow.Cells[3].Value.ToString();
            modAcquirenteForm.textBox4.Text = this.dataGridView1.CurrentRow.Cells[4].Value.ToString();
            modAcquirenteForm.textBox5.Text = this.dataGridView1.CurrentRow.Cells[5].Value.ToString();
            modAcquirenteForm.textBox6.Text = this.dataGridView1.CurrentRow.Cells[6].Value.ToString();
            modAcquirenteForm.textBox7.Text = this.dataGridView1.CurrentRow.Cells[7].Value.ToString();
            modAcquirenteForm.Show();
            
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }
    }
}