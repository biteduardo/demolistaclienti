﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Globalization;
using System.Windows.Forms;

namespace DemoListaClienti
{
    public partial class InsAcquirente : Form
    {
        public delegate void DataChangedEventHandler(object sender, EventArgs e);
        public event DataChangedEventHandler DataChanged;
        string connectionString, query;
        MySqlConnection conn;
        MySqlCommand cmd;

        public InsAcquirente()
        {
            InitializeComponent();
        }

        private void btnConferma_Click(object sender, EventArgs e)
        {            
            connectionString = "server=localhost;database=domodatabase;uid=root;pwd=eduardo;";
            conn = new MySqlConnection(connectionString);
            query = "insert into acquirenti(nome,indirizzo,cap,citta,provincia,longitudine,latitudine) values (@nome,@indirizzo,@cap,@citta,@provincia,@longitudine,@latitudine);";
            try
            {
                conn.Open();
                cmd = new MySqlCommand(query, conn);
                if (textBox1.Text == "")
                    cmd.Parameters.AddWithValue("@nome", "");
                else
                    cmd.Parameters.AddWithValue("@nome", textBox1.Text);
                if (textBox2.Text == "")
                    cmd.Parameters.AddWithValue("@indirizzo", "");
                else
                    cmd.Parameters.AddWithValue("@indirizzo", textBox2.Text);
                if (textBox3.Text == "")
                    cmd.Parameters.AddWithValue("@cap", null);
                else
                    cmd.Parameters.AddWithValue("@cap", textBox3.Text);
                if (textBox4.Text == "")
                    cmd.Parameters.AddWithValue("@citta", "");
                else
                    cmd.Parameters.AddWithValue("@citta", textBox4.Text);
                if (textBox5.Text == "")
                    cmd.Parameters.AddWithValue("@provincia", "");
                else
                    cmd.Parameters.AddWithValue("@provincia", textBox5.Text);
                if (textBox6.Text == "")
                    cmd.Parameters.AddWithValue("@longitudine", null);
                else
                    cmd.Parameters.AddWithValue("@longitudine", textBox6.Text);             
                if (textBox7.Text == "")
                    cmd.Parameters.AddWithValue("@latitudine", null);
                else
                    cmd.Parameters.AddWithValue("@latitudine", textBox7.Text);
                  
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Can not open connection ! ");
                throw;
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                    Close();
                }
            }

            DataChangedEventHandler handler = DataChanged;

            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        private void btnAnnulla_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }
        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }
        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }
        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }
        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void InsAcquirente_Load(object sender, EventArgs e)
        {

        }

        
    }
}
