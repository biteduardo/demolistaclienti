﻿using System;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace DemoListaClienti
{
    public partial class frmListaNodi : Form
    {
        MySqlConnection con;
        MySqlCommand cmd;
        MySqlDataAdapter adapter;
        DataTable dt;
        string connectionString, query;

        public frmListaNodi()
        {
            InitializeComponent();
            refresh();
            btnElimNodo.Enabled = false;
            btnModNodo.Enabled = false;
        }

        private void populate(String id, string indirizzo, String posizione, string note, string periodo)
        {            
            dataGridView1.Rows.Add(id, indirizzo, posizione, note, periodo);
        }

        private void refresh()
        {
            connectionString = "Server=localhost;Database=domodatabase;Uid=root;Pwd=eduardo;";
            con = new MySqlConnection(connectionString);
            dataGridView1.Rows.Clear();
            dt = new DataTable();            
            if(textBox6.Text != "")
                query = "SELECT * FROM nodi where periodo = "+textBox6.Text;
            else
                query = "SELECT * FROM nodi ";
            cmd = new MySqlCommand(query, con);

            try
            {
                con.Open();
                adapter = new MySqlDataAdapter(cmd);
                adapter.Fill(dt);
                foreach (DataRow row in dt.Rows)
                {
                    populate(row[0].ToString(), row[1].ToString(), row[2].ToString(), row[3].ToString(), row[4].ToString());
                }
                con.Close();
                dt.Rows.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (dataGridView1.CurrentCell != null)
                    dataGridView1.CurrentCell.Selected = false;
                textBox1.Text = textBox2.Text = textBox3.Text = textBox4.Text = "";
                con.Close();
            }
        }       

        private void btnInsNodo_Click(object sender, EventArgs e)
        {
            InsNodo insNodoForm = new InsNodo();
            insNodoForm.DataChanged += refreshData;
            insNodoForm.Show();
            btnModNodo.Enabled = false;
            btnElimNodo.Enabled = false;
        }

        private void refreshData(object sender, EventArgs e)
        {
            refresh();
        }   

        private void delete(int id)
        {
            connectionString = "Server=localhost;Database=domodatabase;Uid=root;Pwd=eduardo;";
            con = new MySqlConnection(connectionString);
            dt = new DataTable();            
            query = "DELETE FROM nodi WHERE ID=" + id + "";
            cmd = new MySqlCommand(query, con);
            
            try
            {
                con.Open();
                adapter = new MySqlDataAdapter(cmd);
                adapter.DeleteCommand = con.CreateCommand();
                adapter.DeleteCommand.CommandText =query;                
                if (MessageBox.Show("Conferma eliminazione ID "+id.ToString(), "DELETE", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                {
                    if (cmd.ExecuteNonQuery() > 0)
                    {
                        MessageBox.Show("ID " + id.ToString() + " eliminato");
                        refresh();
                    }
                }
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                con.Close();
            }
        }

        private void btnElimNodo_Click(object sender, EventArgs e)
        {
            String selected = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            int id = Convert.ToInt32(selected);
            delete(id);
            btnElimNodo.Enabled = false;
            btnModNodo.Enabled = false;
        }

        private void update(int id)
        {
            connectionString = "Server=localhost;Database=domodatabase;Uid=root;Pwd=eduardo;";
            con = new MySqlConnection(connectionString);
            dt = new DataTable();
            string posizione,note;
            int indirizzo;
            if (textBox2.Text == "0" || textBox2.Text == null || textBox2.Text == "")
                indirizzo = 0;
            else
                indirizzo = Convert.ToInt32(textBox4.Text);
            if (textBox3.Text == null || textBox3.Text == "")
                posizione = "";
            else
                posizione = textBox3.Text;
            if (textBox4.Text == null || textBox4.Text == "")
                note = "";
            else
                note = textBox4.Text;           
            /*if (textBox5.Text == "0" || textBox5.Text == null || textBox5.Text == "")
                periodo = Convert.ToInt32(DBNull.Value);
            else
                periodo = Convert.ToInt32(textBox5.Text);*/

            query = "UPDATE nodi SET " +
                "indirizzo=" + indirizzo +
                ",posizione='" + posizione +
                "',note='" + note +                          
                "' WHERE id=" + id + "";
            cmd = new MySqlCommand(query, con);
            try
            {
                con.Open();
                adapter = new MySqlDataAdapter(cmd);
                adapter.UpdateCommand = con.CreateCommand();
                adapter.UpdateCommand.CommandText = query;
                if (cmd.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Nodo " + id.ToString() + " aggiornato");
                    refresh();
                }
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                con.Close();
            }
        }

        private void btnModNodo_Click(object sender, EventArgs e)
        {
            /* Cercare di evitare l'eccezione */
            
            String selected = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            int id = Convert.ToInt32(selected);        
            update(id);
            btnElimNodo.Enabled = false;
            btnModNodo.Enabled = false;
        }


        private void btnFilter_Click(object sender, EventArgs e)
        {
            refresh();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            textBox6.Text = "";
            refresh();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int indexRow = e.RowIndex;
            if (indexRow < 0)
                return;
            DataGridViewRow row = dataGridView1.Rows[indexRow];
            textBox1.Text = row.Cells[0].Value.ToString();
            textBox2.Text = row.Cells[1].Value.ToString();
            textBox3.Text = row.Cells[2].Value.ToString();
            textBox4.Text = row.Cells[3].Value.ToString();
            textBox5.Text = row.Cells[4].Value.ToString();
            btnModNodo.Enabled = true;
            btnElimNodo.Enabled = true;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int indexRow = e.RowIndex;
            if (indexRow < 0)
                return;
            DataGridViewRow row = dataGridView1.Rows[indexRow];
            textBox1.Text = row.Cells[0].Value.ToString();
            textBox2.Text = row.Cells[1].Value.ToString();
            textBox3.Text = row.Cells[2].Value.ToString();
            textBox4.Text = row.Cells[3].Value.ToString();
            textBox5.Text = row.Cells[4].Value.ToString();
            btnModNodo.Enabled = true;
            btnElimNodo.Enabled = true;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void frmListaNodi_Load(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellDoubleClick(object sender, MouseEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, MouseEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ModNodo modNodoForm = new ModNodo();
            
            modNodoForm.textBox1.Text = this.dataGridView1.CurrentRow.Cells[2].Value.ToString();
            modNodoForm.textBox2.Text = this.dataGridView1.CurrentRow.Cells[3].Value.ToString();
            modNodoForm.Show();
        }
    }
}
