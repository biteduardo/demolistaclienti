﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Windows.Forms;

namespace DemoListaClienti
{
    public partial class InsPeriodo : Form
    {
        public delegate void DataChangedEventHandler(object sender, EventArgs e);
        public event DataChangedEventHandler DataChanged;
        string connectionString, query;
        MySqlConnection conn;
        MySqlCommand cmd;
        MySqlDataReader myReader;

        public InsPeriodo()
        {
            InitializeComponent();
        }

        private void btnConferma_Click(object sender, EventArgs e)
        {
            connectionString = "server=localhost;database=domodatabase;uid=root;pwd=eduardo;";
            conn = new MySqlConnection(connectionString);
            query = "insert into periodi(numeroinstallazione,inizio,acquirente) values (@numeroinstallazione,@inizio,@acquirente);";
            try
            {
                conn.Open();
                cmd = new MySqlCommand(query, conn);
                if (textBox1.Text == "")
                    cmd.Parameters.AddWithValue("@numeroinstallazione", DBNull.Value);
                else
                    cmd.Parameters.AddWithValue("@numeroinstallazione", textBox1.Text);
                cmd.Parameters.AddWithValue("@inizio", dateTimePicker1.Text);
                if (comboBox1.SelectedIndex >= 0)
                    cmd.Parameters.AddWithValue("@acquirente", comboBox1.Text);
                
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Can not open connection ! ");
                Close();
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                    Close();
                }
            }
            DataChangedEventHandler handler = DataChanged;

            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        private void InsPeriodo_Load(object sender, EventArgs e)
        {
            connectionString = "server=localhost;database=domodatabase;uid=root;pwd=eduardo;";
            conn = new MySqlConnection(connectionString);
            query = "select * from acquirenti";
            cmd = new MySqlCommand(query, conn);
            try
            {
                conn.Open();
                myReader = cmd.ExecuteReader();
                while (myReader.Read())
                {
                    comboBox1.Items.Add(myReader.GetInt32(0));
                }
                myReader.Close();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("-------------------------------------");
            }
        }

        private void btnAnnulla_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
