﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Windows.Forms;

namespace DemoListaClienti
{
    public partial class InsNodo : Form
    {
        public delegate void DataChangedEventHandler(object sender, EventArgs e);
        public event DataChangedEventHandler DataChanged;
        string connectionString, query,check;
        MySqlConnection conn;
        MySqlCommand cmd;
        MySqlDataReader myReader;
        
        public InsNodo()
        {
            InitializeComponent();
        }

        private void btnConferma_Click(object sender, EventArgs e)
        {
            connectionString = "server=localhost;database=domodatabase;uid=root;pwd=eduardo;";
            conn = new MySqlConnection(connectionString);
            query = "insert into nodi(indirizzo,posizione,note,periodo) values (@indirizzo,@posizione,@note,@periodo);";


            try
            {
                conn.Open();
                cmd = new MySqlCommand(query, conn);                    
                if (comboBox1.SelectedIndex >= 0)
                        cmd.Parameters.AddWithValue("@periodo", comboBox1.Text);             
                if (textBox1.Text == "")
                    cmd.Parameters.AddWithValue("@posizione", "");
                else
                    cmd.Parameters.AddWithValue("@posizione", textBox1.Text);
                if (textBox2.Text == "")
                    cmd.Parameters.AddWithValue("@note", "");
                else
                    cmd.Parameters.AddWithValue("@note", textBox2.Text);

                /* Per ora, l'indirizzo del nodo è pari a zero. E' richiesto
                 * che non ce ne sia più di uno per lo stesso periodo*/
                if (textBox3.Text == "")
                    cmd.Parameters.AddWithValue("@indirizzo", 0);
                else
                    cmd.Parameters.AddWithValue("@indirizzo", textBox3.Text);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Can not open connection ! " + comboBox1.SelectedIndex);
                Close();
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                    Close();
                }
            }
            DataChangedEventHandler handler = DataChanged;

            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        private void InsNodo_Load(object sender, EventArgs e)
        {
            connectionString = "server=localhost;database=domodatabase;uid=root;pwd=eduardo;";
            conn = new MySqlConnection(connectionString);
            query = "select * from periodi";
            cmd = new MySqlCommand(query, conn);
            try
            {
                conn.Open();
                myReader = cmd.ExecuteReader();
                while (myReader.Read())
                {
                    comboBox1.Items.Add(myReader.GetInt32(0));
                }
                myReader.Close();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("-------------------------------------");
            }            
        }

        private void btnAnnulla_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
