﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoListaClienti
{
    public partial class ModAcquirente : Form
    {
        public delegate void DataChangedEventHandler(object sender, EventArgs e);
        public event DataChangedEventHandler DataChanged;
        string connectionString, query;
        MySqlConnection conn;
        MySqlCommand cmd;


        public ModAcquirente()
        {
            InitializeComponent();
        }

        private void btnConferma_Click(object sender, EventArgs e)
        {
            connectionString = "Server=localhost;Database=domodatabase;Uid=root;Pwd=eduardo;";
            conn = new MySqlConnection(connectionString);
            query = "UPDATE acquirenti SET " +
                "nome='" + textBox1.Text +
                "',indirizzo='" + textBox2.Text +
                "',cap=" + textBox3.Text +
                ",citta='" + textBox4.Text +
                "',provincia='" + textBox5.Text +
                "',longitudine='" + textBox6.Text +
                "',latitudine='" + textBox7.Text +
                "' WHERE id=" + textBox8.Text + "";
            try
            {
                conn.Open();
                cmd = new MySqlCommand(query, conn);
                if (cmd.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Dati acquirente " + textBox8.Text + " aggiornati");
                    
                }                
                    

            }
            catch
            {
                MessageBox.Show("Can not open connection ! ");
            }
            finally
            {
                conn.Close();
                Close();
            }

            DataChangedEventHandler handler = DataChanged;

            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        private void btnAnnulla_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void ModAcquirente_Load(object sender, EventArgs e)
        {

        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }


    }
}
