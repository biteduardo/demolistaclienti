﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Windows.Forms;

namespace DemoListaClienti
{
    public partial class frmListaPeriodi : Form
    {
        MySqlConnection con;
        MySqlCommand cmd;
        MySqlDataAdapter adapter;
        DataTable dt;
        string connectionString, query;

        public frmListaPeriodi()
        {
            InitializeComponent();            
            refresh();
            btnElimPeriodo.Enabled = false;
            btnModPeriodo.Enabled = false;
        }
   
        private void populate(String id, String numeroinstallazione, string inizio, string acquirente)
        {
            DateTime date = DateTime.ParseExact(inizio, "dd/MM/yyyy HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);
            inizio = date.ToString("yyyy-MM-dd");
            dataGridView1.Rows.Add(id, numeroinstallazione, inizio, acquirente);
        }

        private void refresh()
        {
            string conString = "Server=localhost;Database=domodatabase;Uid=root;Pwd=eduardo;";
            con = new MySqlConnection(conString);       
            dt = new DataTable();
            dataGridView1.Rows.Clear();
            string sql = "SELECT * FROM periodi ";
            cmd = new MySqlCommand(sql, con);
            try
            {
                con.Open();
                adapter = new MySqlDataAdapter(cmd);
                adapter.Fill(dt);
                foreach (DataRow row in dt.Rows)
                {
                    populate(row[0].ToString(), row[1].ToString(), row[2].ToString(), row[3].ToString());
                }
                con.Close();
                dt.Rows.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
            finally
            {
                if (dataGridView1.CurrentCell != null)
                    dataGridView1.CurrentCell.Selected = false;
                textBox1.Text = textBox2.Text = dateTimePicker1.Text = textBox4.Text = "";
                con.Close();
            }
        }
        
        private void btnInsPeriodo_Click(object sender, EventArgs e)
        {
            InsPeriodo insPeriodoForm = new InsPeriodo();
            insPeriodoForm.DataChanged += refreshData;
            insPeriodoForm.Show();
            btnModPeriodo.Enabled = false;
            btnElimPeriodo.Enabled = false;

        }

        private void refreshData(object sender, EventArgs e)
        {
            refresh();
        }

        private void delete(int id)
        {
            string conString = "Server=localhost;Database=domodatabase;Uid=root;Pwd=eduardo;";
            con = new MySqlConnection(conString);
            dt = new DataTable();      
            string sql = "DELETE FROM periodi WHERE ID=" + id + "";
            cmd = new MySqlCommand(sql, con);
            try
            {
                con.Open();
                adapter = new MySqlDataAdapter(cmd);
                adapter.DeleteCommand = con.CreateCommand();
                adapter.DeleteCommand.CommandText = sql;
                if (MessageBox.Show("Conferma eliminazione ID " + id.ToString(), "DELETE", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                {
                    if (cmd.ExecuteNonQuery() > 0)
                    {
                        MessageBox.Show("ID " + id.ToString() + " eliminato");
                        refresh();
                    }
                }
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                con.Close();
            }
        }

        private void btnElimPeriodo_Click(object sender, EventArgs e)
        {
            String selected = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            int id = Convert.ToInt32(selected);
            delete(id);
            btnElimPeriodo.Enabled = false;
            btnModPeriodo.Enabled = false;
        }

        private void update(int id)
        {
            connectionString = "Server=localhost;Database=domodatabase;Uid=root;Pwd=eduardo;";
            con = new MySqlConnection(connectionString);
            dt = new DataTable();
            string inizio;
            int numeroinstallazione;

            if (textBox2.Text == "0" || textBox2.Text == null || textBox2.Text == "")
                numeroinstallazione = 0;
            else
                numeroinstallazione = Convert.ToInt32(textBox2.Text);

            inizio = dateTimePicker1.Text;

            /*if (textBox3.Text == null || textBox3.Text == "")
                inizio = Convert.ToDateTime("0000-00-00");
            else
                inizio = Convert.ToDateTime(textBox3.Text);*/
            /*
            if (textBox4.Text == "0" || textBox4.Text == null || textBox4.Text == "")
                acquirente = 0;
            else
                acquirente = Convert.ToInt32(textBox4.Text);*/

            query = "UPDATE periodi SET " +
                "numeroinstallazione=" + numeroinstallazione +
                ",inizio='" + inizio +             
                "' WHERE id=" + id + "";

            cmd = new MySqlCommand(query, con);
            try
            {
                con.Open();
                adapter = new MySqlDataAdapter(cmd);
                adapter.UpdateCommand = con.CreateCommand();
                adapter.UpdateCommand.CommandText = query;
                if (cmd.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Periodo " + id.ToString() + " aggiornato");
                    refresh();
                }                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);                
            }
            finally
            {                
                con.Close();
            }
        }
        
        private void btnModPeriodo_Click(object sender, EventArgs e)
        {
            /* Cercare di evitare l'eccezione */
            String selected = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            int id = Convert.ToInt32(selected);
            update(id);
            btnElimPeriodo.Enabled = false;
            btnModPeriodo.Enabled = false;
        }
        
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int indexRow = e.RowIndex;
            if (indexRow < 0)
                return;
            DataGridViewRow row = dataGridView1.Rows[indexRow];
            textBox1.Text = row.Cells[0].Value.ToString();
            textBox2.Text = row.Cells[1].Value.ToString();            
            dateTimePicker1.Text = row.Cells[2].Value.ToString();            
            textBox4.Text = row.Cells[3].Value.ToString();
            btnModPeriodo.Enabled = true;
            btnElimPeriodo.Enabled = true;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int indexRow = e.RowIndex;
            if (indexRow < 0)
                return;
            DataGridViewRow row = dataGridView1.Rows[indexRow];
            
            textBox1.Text = row.Cells[0].Value.ToString();
            textBox2.Text = row.Cells[1].Value.ToString();
            dateTimePicker1.Text = row.Cells[2].Value.ToString();
            textBox4.Text = row.Cells[3].Value.ToString();
            btnModPeriodo.Enabled = true;
            btnElimPeriodo.Enabled = true;            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void frmListaPeriodi_Load(object sender, EventArgs e)
        {

        }

        private void txtBoxPeriodi_TextChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            
            
        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ModPeriodo modPeriodoForm = new ModPeriodo();
            modPeriodoForm.textBox1.Text = this.dataGridView1.CurrentRow.Cells[1].Value.ToString();
            modPeriodoForm.dateTimePicker1.Text = this.dataGridView1.CurrentRow.Cells[2].Value.ToString();
            modPeriodoForm.Show();
        }

        private void dataGridView1_CellContentDoubleClick(object sender, MouseEventArgs e)
        {

        }

        private void dataGridView1_CellContentDoubleClick(object sender, EventArgs e)
        {
        
            
        }

        private void dataGridView1_CellContentClick(object sender, MouseEventArgs e)
        {

        }

        

       
    }
}
